--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:52:26 05/11/2015
-- Design Name:   
-- Module Name:   /home/doublechooz/SiPMKannen/DRS4/firmware/DRS4Trigger//TRG_TIME_tb.vhd
-- Project Name:  DRS4Trigger_ISE10.ISE
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: TRG_TIME
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
ENTITY TRG_TIME_tb IS
END TRG_TIME_tb;
 
ARCHITECTURE behavior OF TRG_TIME_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT TRG_TIME
    PORT(
         CLK : IN  std_logic;
         RES : IN  std_logic;
         TIME_CO : OUT  std_logic_vector(63 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal RES : std_logic := '0';

 	--Outputs
   signal TIME_CO : std_logic_vector(63 downto 0);
 
  constant CLK_period :time := 20 ns;

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: TRG_TIME PORT MAP (
          CLK => CLK,
          RES => RES,
          TIME_CO => TIME_CO
        );
 
   -- No clocks detected in port list. Replace CLK below with 
   -- appropriate port name 
  
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      report "Starting";
      wait for 10ns;	

      report "Init";
			RES <= '1' ;
      wait for CLK_period;

      report "Clear resets";
			RES <= '0';

      report "Wait 10 clk";
      wait for CLK_period*10;

ASSERT ( TIME_CO = std_logic_vector(to_unsigned(10,32)) )
 report "TIME_CO /= 10"
 severity ERROR;


      report "Wait 7 clk";
      wait for CLK_period*7;

ASSERT ( TIME_CO = std_logic_vector(to_unsigned(17,32)) )
 report "TIME_CO /= 17"
 severity ERROR;

      report "Init";
			RES <= '1' ;
      wait for CLK_period;

      report "Clear resets";
			RES <= '0';

      report "Wait 12 clk";
      wait for CLK_period*12;

ASSERT ( TIME_CO = std_logic_vector(to_unsigned(10,32)) )
 report "TIME_CO /= 12"
 severity ERROR;


      wait for CLK_period*2;


      wait;
   end process;

END;
