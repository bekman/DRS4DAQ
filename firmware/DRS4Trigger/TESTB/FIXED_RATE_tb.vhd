--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:47:36 09/10/2015
-- Design Name:   
-- Module Name:   /home/doublechooz/SiPMKannen/DRS4/firmware/DRS4Trigger/TESTB//FIXED_RATE_tb.vhd
-- Project Name:  DRS4Trigger_ISE10.ISE
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: FIXED_RATE
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
ENTITY FIXED_RATE_tb IS
END FIXED_RATE_tb;
 
ARCHITECTURE behavior OF FIXED_RATE_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT FIXED_RATE
    PORT(
         DRV : IN  std_logic;
         CLK : IN  std_logic;
         RES : IN  std_logic;
         RATE_IN : IN  std_logic_vector(15 downto 0);
         TRG : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal DRV : std_logic := '0';
   signal CLK : std_logic := '0';
   signal RES : std_logic := '0';
   signal RATE_IN : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal TRG : std_logic;
   constant CLK_period :time := 16.6667 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: FIXED_RATE PORT MAP (
          DRV => DRV,
          CLK => CLK,
          RES => RES,
          RATE_IN => RATE_IN,
          TRG => TRG
        );
 
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 
   DRV_process :process
   begin
		wait for CLK_period/2;
		DRV <= '0';
		wait for CLK_period*2;
		DRV <= '1';
		wait for CLK_period*1.5;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      report "Starting";
      wait for 10ns;	

      report "Init w/o rate";
			RES <= '1' ;
			RATE_IN <= X"0000";
      wait for CLK_period;

      report "Clear resets";
			RES <= '0';
      wait for 3ns;

      report "Observe Output";
      wait for 100ns;
      
      report "Init w rate";
			RES <= '1' ;
			RATE_IN <= X"0001" ;
      wait for CLK_period;

      report "Clear resets";
			RES <= '0';
      wait for 3ns;

      report "Observe Output";
      wait for 200ns;

      report "Init w rate";
			RES <= '1' ;
			RATE_IN <= X"0003" ;
      wait for CLK_period;

      report "Clear resets";
			RES <= '0';
      wait for 3ns;

      report "Observe Output";
      wait for 600ns;
      
      report "Init w rate";
			RES <= '1' ;
			RATE_IN <= X"0006" ;
      wait for CLK_period;

      report "Clear resets";
			RES <= '0';
      wait for 3ns;

      report "Observe Output";
      wait for 600ns;
      

      wait;
   end process;

END;
