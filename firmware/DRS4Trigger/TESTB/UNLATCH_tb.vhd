--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:50:44 09/12/2015
-- Design Name:   
-- Module Name:   /home/data/SiPM/firmware/DRS4Trigger/TESTB/UNLATCH_tb.vhd
-- Project Name:  DRS4Trigger_ISE14.ISE
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: UNLATCH
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
 
ENTITY UNLATCH_tb IS
END UNLATCH_tb;
 
ARCHITECTURE behavior OF UNLATCH_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT UNLATCH
    PORT(
         DRV : IN  std_logic;
         CLK : IN  std_logic;
         EDG : IN  std_logic;
         DRV_O : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal DRV : std_logic := '0';
   signal CLK : std_logic := '0';
   signal EDG : std_logic := '0';

 	--Outputs
   signal DRV_O : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 16.6667 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: UNLATCH PORT MAP (
          DRV => DRV,
          CLK => CLK,
          EDG => EDG,
          DRV_O => DRV_O
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 
   DRV_process :process
   begin
		wait for CLK_period/3;
		DRV <= '0';
		wait for CLK_period*2;
		DRV <= '1';
		wait for CLK_period*2/3;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		

      wait for 200 ns;	
--      report "Shoot";
--			DRV <= '1' ;
--      wait for 400ns;
      report "Change polarity";
			EDG <= '1' ;
      wait for 400ns;


      -- insert stimulus here 

      wait;
   end process;

END;
