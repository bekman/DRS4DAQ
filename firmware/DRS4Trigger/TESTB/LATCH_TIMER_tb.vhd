--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:51:54 05/13/2015
-- Design Name:   
-- Module Name:   /home/doublechooz/SiPMKannen/DRS4/firmware/DRS4Trigger/TESTB/LATCH_TIMER_tb.vhd
-- Project Name:  DRS4Trigger_ISE10.ISE
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: LATCH_TIMER
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
ENTITY LATCH_TIMER_tb IS
END LATCH_TIMER_tb;
 
ARCHITECTURE behavior OF LATCH_TIMER_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT LATCH_TIMER
    PORT(
         CLK : IN  std_logic;
         RES : IN  std_logic;
         TRG : IN  std_logic;
         LATCHDELAY : IN  std_logic_vector(7 downto 0);
         TRGLATCH : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal RES : std_logic := '0';
   signal TRG : std_logic := '0';
   signal LATCHDELAY : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal TRGLATCH : std_logic;
 
   constant CLK_period :time := 20 ns;

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: LATCH_TIMER PORT MAP (
          CLK => CLK,
          RES => RES,
          TRG => TRG,
          LATCHDELAY => LATCHDELAY,
          TRGLATCH => TRGLATCH
        );
 
   -- No clocks detected in port list. Replace CLK below with 
   -- appropriate port name 
 
   
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      report "Starting";
      wait for 10ns;	

      report "Init w/o delay";
			RES <= '1' ;
			LATCHDELAY <= "00000000" ;
      wait for CLK_period;

      report "Clear resets";
			RES <= '0';
      wait for 3ns;

      report "Shoot Long";
			TRG <= '1';
      wait for 60ns;
			TRG <= '0';
      wait for 10ns;

      report "Wait 3 clk";
      wait for CLK_period*3;

      report "Shoot Short";
			TRG <= '1';
      wait for 10ns;
			TRG <= '0';
      wait for 10ns;

      report "Wait 3 clk";
      wait for CLK_period*4;

      report "Init w/ delay";
			RES <= '1' ;
			LATCHDELAY <= "00001100" ;
      wait for CLK_period;

      report "Clear resets";
			RES <= '0';
      wait for 3ns;

      report "Shoot";
			TRG <= '1';
      wait for 40ns;
			TRG <= '0';
      wait for 10ns;

      report "Wait 15 clk";
      wait for CLK_period*15;

      report "Shoot";
			TRG <= '1';
      wait for 40ns;
			TRG <= '0';
      wait for 10ns;

      report "Wait 5 clk";
      wait for CLK_period*5;
			
      report "Breaking Reset";
			RES <= '1' ;
      wait for CLK_period;
			RES <= '0';
      wait for 3ns;

      report "Wait 5 clk";
      wait for CLK_period*5;

      report "Shoot Short";
			TRG <= '1';
      wait for 10ns;
			TRG <= '0';
      wait for 10ns;

      report "Wait 10 clk";
      wait for CLK_period*10;


      wait;
   end process;

END;
