----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:44:58 05/11/2015 
-- Design Name: 
-- Module Name:    SCALER - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SCALER is
--  generic ( SCALER_DEPTH: natural	:= 32 );
    Port ( CLK : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           SCALER_CO : out  STD_LOGIC_VECTOR );
--           SCALER_CO : out  STD_LOGIC_VECTOR (SCALER_DEPTH-1 downto 0));
end SCALER;

architecture Behavioral of SCALER is
signal SCALER_CO_I : std_logic_vector (SCALER_CO'range);  

begin
-------------------------------
-- Resetable Scaler Counter  --
-------------------------------
process (CLK, RES) 
begin
  if RES = '1' then 
    SCALER_CO_I (SCALER_CO'range) <= (others => '0'); -- <= (others => '1');
--    SCALER_CO_I ( 3 downto 0) <= "0000"; -- <= "1110"; 
  elsif (CLK = '1' and CLK 'event) then
    SCALER_CO_I <= SCALER_CO_I + 1;
  end if;
end process;
	

SCALER_CO <= SCALER_CO_I;


end Behavioral;

