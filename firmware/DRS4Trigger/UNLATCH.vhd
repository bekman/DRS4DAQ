----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09/12/2015 
-- Design Name: 
-- Module Name:    UNLATCH - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity UNLATCH is
  Port ( 
          DRV :  in  STD_LOGIC; -- Driver
          CLK :  in  STD_LOGIC; -- Duration Clock
			 EDG :  in  STD_LOGIC; -- Clock Edge
			 DRV_O: out STD_LOGIC
        );
end UNLATCH;

architecture arc_UNLATCH of UNLATCH is
	signal DRVD       : std_logic := '0'; -- was '1' at last CLK EGD
begin
	DRV_O <= DRV and not DRVD;--  and (not RES);

	DRVD_proc: process(CLK, DRV)
	begin
		if (CLK'event and CLK = EDG) then
			if ( DRV = '1' ) then
				DRVD <= '1';
			else
				DRVD <= '0';
			end if;
		end if;
	end process;

end arc_UNLATCH;

