----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    2015-10-08 
-- Design Name: 
-- Module Name:    FIXED_RATE - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FIXED_RATE is
  Port ( 
          DRV : in  STD_LOGIC; -- Driver Clock
          CLK : in  STD_LOGIC; -- TRG duration Clock
          RES : in  STD_LOGIC; -- Reset and Hold
          RATE_IN : in  STD_LOGIC_VECTOR (15 downto 0);
          TRG : out  STD_LOGIC -- Trigger Out
        );
end FIXED_RATE;

architecture arc_FIXED_RATE of FIXED_RATE is
	signal DRV_B      : std_logic := '0';
	signal DRV_S      : std_logic := '0';
	signal DRV_POS    : std_logic := '0';
	signal DRV_NEG    : std_logic := '0';
	signal COUNT      : std_logic_vector (RATE_IN'range) := (others => '0'); -- +1 to delay -- RATE_IN'high+1
	signal RATE_1     : std_logic := '1'; 
	signal TRG_ON     : std_logic := '0';
	signal TRGD       : std_logic := '0'; --has triggered
	signal TRG_I      : std_logic := '0';

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;
  attribute BOX_TYPE : STRING ;
  attribute BOX_TYPE of BUFG : component is "PRIMITIVE";



begin

--  inst_bufg_DRV: BUFG
--    port map (
--      I => DRV,
--      O => DRV_B
--    );

	DRV_SYNC_proc: process (DRV, CLK)
		variable resync : std_logic_vector(1 to 3);
	begin
		if (CLK'event and CLK='0') then
			DRV_S <= DRV;
			DRV_POS <= resync(2) and not resync(3);
			DRV_NEG <= resync(3) and not resync(2);
			resync := DRV & resync(1 to 2);
		end if;
	end process;


	TRG <= TRG_I and TRG_ON and (not RES);

--	RATE_1 <= '1' when (COUNT = X"0001" and DRV_S = '1') else '0';
	
	RATE_1 <= '1' when (COUNT = X"0000") and TRG_ON = '1' else '0';
	
	TRG_ON <= '0' when RATE_IN = (RATE_IN'range => '0') else '1'; 

	
	COUNT_proc: process (RES,CLK, DRV_POS, TRG_ON, RATE_IN)
	begin
		if (CLK'event and CLK='1') then
			if ( RES = '1' or TRG_ON = '0' or (RATE_1 = '1' and DRV_POS = '1') ) then
				COUNT <= RATE_IN ; --( others => '0' );
			elsif ( DRV_POS = '1' ) then --full period
				COUNT <= COUNT - 1 ; --count down
			end if;	
		end if;
	end process;

	TRG_I <= TRGD;--  and (not RES);

-- RATE_1 starts at rising_edge, 
-- ackn arrival at falling_edge (TRGD)
	TRGD_proc: process(CLK, RATE_1, RES, TRG_ON)
	begin
		if ( RES = '1' or TRG_ON = '0' ) then
			TRGD <= '0';
		elsif ( CLK'event and CLK = '0' ) then
			TRGD <= RATE_1;
		end if;
	end process;

	


end arc_FIXED_RATE;

