----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:20:13 05/13/2015 
-- Design Name: 
-- Module Name:    LATCH_TIMER - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity LATCH_TIMER is
    Port ( CLK : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           TRG : in  STD_LOGIC;
           LATCHDELAY : in  STD_LOGIC_VECTOR (7 downto 0);
           TRGLATCH : out  STD_LOGIC);
end LATCH_TIMER;

architecture arc_LATCH_TIMER of LATCH_TIMER is
	--signal LATCH_CO : std_logic_vector (LATCHDELAY'high+1 downto 0) := (others => '0'); -- +1 to delay -- LATCHDELAY'high+1
	signal LATCH_CO : std_logic_vector (8 downto 0) := (others => '0'); -- +1 to delay -- LATCHDELAY'high+1
	signal TRGLATCH_I : std_logic := '0';
	signal TRG_D : std_logic := '0';
	signal LATCH_ENA : std_logic := '0';
  signal drs_trg_delay : std_logic_vector(128 downto 0);
	attribute keep: string;
  attribute keep of drs_trg_delay : signal is "true";


  component LUT1
    generic (
      INIT : bit_vector
    );
    port(
      O : out STD_ULOGIC;
      I0 : in STD_ULOGIC
    );
  end component;  

begin

	TRGLATCH <= TRGLATCH_I;

--	TRGLATCH_I <= TRG; --DEBUG

	LATCH_ENA <= '0' when (LATCHDELAY = (LATCHDELAY'range => '0')) else '1';

--	TRG_D <= TRG;
	
	process (RES, CLK, TRG)
	begin
		if ( TRG_D = '1' and CLK = '0') then
			TRG_D <= '0';
		elsif ( TRG'event and TRG = '1' ) then
			TRG_D <= '1';
		end if;
	end process;

	process (RES, CLK, TRG_D, LATCH_ENA, LATCH_CO, LATCHDELAY)
	begin
    -- if any latching time set (inp_del != (000...0))			
		if ( RES = '1' or LATCH_ENA = '0' ) then
			LATCH_CO <= ( others => '0' );
		elsif ( TRG_D = '1' ) then
			LATCH_CO <= ('1' & LATCHDELAY);
		elsif (rising_edge(CLK)) then
			if (LATCH_CO(LATCH_CO'high) = '1') then
				LATCH_CO <= LATCH_CO - 1 ; --count down
			else  -- if MSB is off means counter has substracted 1 from (1000...0)
				LATCH_CO (LATCH_CO'high-1 downto 0) <= ( others => '0' );
			end if;
		end if;
	end process;

	process (TRG, LATCH_CO, LATCH_ENA, LATCHDELAY)
	begin
		if ( LATCH_ENA = '1' ) then
			TRGLATCH_I <= LATCH_CO(LATCH_CO'high);
		else
			TRGLATCH_I <= TRG;
		end if;
	end process;



--  drs_trigger                  <= drs_trg_delay(CONV_INTEGER(drs_ctl_delay_sel)*8);
	
  -- buffer chain delay for hardware trigger
  drs_trg_delay(0)             <= TRG;
  delayed_trig_gen: for bit_no in 1 to drs_trg_delay'high-1 generate
    LUT1_inst : LUT1
    generic map (
      INIT => "10")
    port map (
      O  => drs_trg_delay(bit_no),
      I0 => drs_trg_delay(bit_no-1)
    );
  end generate;


--- V1 Work
--	process (RES, CLK, LATCH_CO, TRG, LATCHDELAY)
--	begin
--		if ( RES = '1' ) then
--			LATCH_CO <= ( others => '0' );
--		else
--			if (LATCHDELAY /= (LATCHDELAY'range => '0')) then -- if any latching time set (inp_del != (000...0)) 
--				if ( TRG = '1' ) then --- if latch time to be added to the signal end
--				-- if ( rising_edge(TRG) ) then --- if latch time starts at signal start --- does not work if CO_MSB == '0' upon rising_edge
--					TRGLATCH_I <= '1';
--					LATCH_CO <= ('1' & LATCHDELAY);
--				else
--					if (LATCH_CO(LATCH_CO'high) = '0') then -- if MSB is off means counter has substracted 1 from (1000...0)
--						TRGLATCH_I <= '0';
--						LATCH_CO <= ( others => '0' );
--					elsif (rising_edge(CLK)) then
--						LATCH_CO <= LATCH_CO - 1 ; --count down
--					--end if;
--					end if;
--				end if;
--			else
--				TRGLATCH_I <= TRG;
--			end if;
--		end if;
--
--	end process;


--- PreWork

--	process (RES)
--	begin
--		if (RES = '1') then
--			LATCH_CO <= ( others => '0' );
--		end if;
--	end process;

--	LATCH_CO <= ( others => '0' ) when RES = '1';
	
--	TRGLATCH_I <= '1' when rising_edge(TRG) else when (LATCH_CO(LATCH_CO'high) = '0'

--- DontWork

--	process (RES, LATCHDELAY, TRG)
--	begin
--	end process;



--  proc_latch_input: process(I_RESET, I_CLK66, drs_hard_inp_unlatched(i))
--  begin
--	  if (I_RESET = '1' or rising_edge(drs_hard_inp_unlatched(i)) then
--      	drs_hard_latch_co(i)(7 downto 0) <= (others => '0');
--      	drs_hard_inp(i) <= '0';
--    	elsif rising_edge(I_CLK66) then
--      	drs_hard_latch_co(i) <= drs_hard_latch_co(i) - 1; -- count down 
--      	-- toggle refclk if timer expires
--      	if ( drs_hard_latch_co(i)(drs_hard_latch_co(i)'high) = '1' ) then
--        	drs_hard_inp(i) <= '0';
--        	drs_hard_latch_co(i)(7 downto 0) <= drs_hard_inp_del(7 downto 0);     -- I_CLK33 is actually a 30 MHz clock
--      	end if;  
--    	end if;
--  end process;

  -- drs_hard_inp_del

--  proc_latch_input: process(I_RESET, I_CLK66, drs_hard_inp_unlatched(i))
--  begin
--	  if (I_RESET = '1') then
--      drs_hard_inp(i) <= '0'; 
--    else
--      if drs_hard_latch_co(i)(drs_hard_latch_co(i)'high) = '0' then 
--				drs_hard_latch_co(i) <= ('1' & drs_hard_inp_del);     -- MSB set to 1, seemless to user
--			end if;
--				
--		  if rising_edge(drs_hard_inp_unlatched(i)) then 
--				drs_hard_inp(i) <= '1';
--			end if;
--
--		  if (drs_hard_inp(i) = '1' and rising_edge(I_CLK66)) then
--        drs_hard_latch_co(i) <= drs_hard_latch_co(i) - 1; -- count down
--      end if;
--
--      if ( drs_hard_inp_del /= (drs_hard_inp_del'range => '0') ) then  -- if any latching time set (inp_del != (000...0))
--			  drs_hard_inp(i) <= drs_hard_latch_co(i)(drs_hard_latch_co(i)'high);   -- if MSB is off means counter has substracted 1 from (1000...0)
--      else
--        drs_hard_inp(i) <= drs_hard_inp_unlatched(i);
--			end if;
--	  end if;
	  
--	  if (I_RESET = '1' or rising_edge(drs_hard_inp_unlatched(i)) then
--      	drs_hard_latch_co(i)(7 downto 0) <= (others => '0');
--      	drs_hard_inp(i) <= '0';
--    	elsif rising_edge(I_CLK66) then
--      	drs_hard_latch_co(i) <= drs_hard_latch_co(i) - 1; -- count down 
--      	-- toggle refclk if timer expires
--      	if ( drs_hard_latch_co(i)(drs_hard_latch_co(i)'high) = '1' ) then
--        	drs_hard_inp(i) <= '0';
--        	drs_hard_latch_co(i)(7 downto 0) <= drs_hard_inp_del(7 downto 0);     -- I_CLK33 is actually a 30 MHz clock
--      	end if;  
--    	end if;
--  end process;


-- SRL16_1: 16-bit shift register LUT operating on  SRL16 positiv of clock
--  proc_latch_input: process(I_CLK66, drs_hard_inp_unlatched(i))
--  begin
--   SRL16_1_INP_DEL_1 : SRL8bit
--   generic map (
--      INIT => X"0000")
--   port map (
--      Q   => drs_hard_inp (i),       -- SRL data output
--      A0  => drs_hard_inp_del(0),     -- Select[0] input
--      A1  => drs_hard_inp_del(1),     -- Select[1] input
--      A2  => drs_hard_inp_del(2),     -- Select[2] input
--      A3  => drs_hard_inp_del(3),     -- Select[3] input
--      CLK => I_CLK66,    -- Clock input
--      D   => drs_hard_inp_unlatched (i)      -- SRL data input
--   );
--  end process;



end arc_LATCH_TIMER;

