----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:44:58 05/11/2015 
-- Design Name: 
-- Module Name:    TIME_COUNTER - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TIME_COUNTER is
  generic ( TIME_DEPTH: natural	:= 64 );
    Port ( CLK : in  STD_LOGIC;
           RES : in  STD_LOGIC;
           TIME_CO : out  STD_LOGIC_VECTOR (TIME_DEPTH-1 downto 0));
end TIME_COUNTER;

architecture Behavioral of TIME_COUNTER is
signal TIME_CO_I : std_logic_vector (TIME_DEPTH-1 downto 0);  

begin
--------------------------
-- Trigger Time Counter --
--------------------------
process (CLK, RES) 
begin
  if RES = '1' then 
    TIME_CO_I (TIME_DEPTH-1 downto 0) <= (others => '0'); -- <= (others => '1');
--    TIME_CO_I ( 3 downto 0) <= "0000"; -- <= "1110"; 
  elsif (CLK = '1' and CLK 'event) then
    TIME_CO_I <= TIME_CO_I + 1;
  end if;
end process;
	

TIME_CO <= TIME_CO_I;


end Behavioral;

