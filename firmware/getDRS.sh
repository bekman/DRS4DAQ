#!/bin/bash

if [ ! -e drs*.tar.gz ]; then
wget http://www.psi.ch/drs/SoftwareDownloadEN/drs-5.0.3.tar.gz
fi
if [ ! -e wxWidgets*.tar.bz2 ]; then
wget https://sourceforge.net/projects/wxwindows/files/3.0.0/wxWidgets-3.0.0.tar.bz2
fi
if [ ! -e libusb*.tar.bz2 ]; then
wget http://netcologne.dl.sourceforge.net/project/libusb/libusb-1.0/libusb-1.0.18/libusb-1.0.18.tar.bz2 
fi

tar -xzf drs*.tar.gz
tar -xjf wxWidgets*.tar.bz2
tar -xjf libusb*.tar.bz2

cd libusb*
PATHusb=$PWD

sudo apt-get install libudev-dev
#./configure
./configure --prefix=$PWD
make; make install
cd ..

cd wxWidgets*
PATHwx=$PWD
sudo apt-get install libgtk2.0-dev
./configure --prefix=$PWD
make; make install
cd ..

cd drs*
export SHELL=/bin/bash
export PATH=$PATHusb:$PATHwx:$PATH
make

if [ -e 41-drs.rules ]; then
sudo cp -v 41-drs.rules /etc/udev/rules.d 
sudo /etc/init.d/udev restart
fi


