/********************************************************************\

  Name:         drs_exam.cpp
  Created by:   Stefan Ritt

  Contents:     Simple example application to read out a DRS4
                evaluation board


\********************************************************************/
#include <signal.h>
#include <math.h>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>

#define DIR_SEPARATOR '/'

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "strlcpy.h"
#include "DRS.h"

/*------------------------------------------------------------------*/
int catch_signal_flag=0;
void catch_signal(int sig){catch_signal_flag=1;};
int main()
{
   signal(SIGINT, catch_signal);
   int i, j, nBoards;
   DRS *drs;
   DRSBoard *b;
   float time_array[8][1024];
   float wave_array[8][1024];
   FILE  *f;

   /* do initial scan */
   drs = new DRS();

   /* show any found board(s) */
   for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
      b = drs->GetBoard(i);
      printf("Found DRS4 evaluation board, serial #%d, firmware revision %d = %X\n", 
         b->GetBoardSerialNumber(), b->GetFirmwareVersion(), b->GetFirmwareVersion());
   }

   /* exit if no board found */
   nBoards = drs->GetNumberOfBoards();
   if (nBoards == 0) {
      printf("No DRS4 evaluation board found\n");
      return 0;
   }

   /* continue working with first board only */
   b = drs->GetBoard(0);

   /* initialize board */
//   b->PrintRegisters();
   b->Init();
   b->PrintRegisters();
	 
	 b->EnableTrigger(0, 0);           // enable hardware trigger

   /* set sampling frequency */
   b->SetFrequency(5, true);

   /* enable transparent mode needed for analog trigger */
   b->SetTranspMode(1);

   /* set input range to -0.5V ... +0.5V */
   b->SetInputRange(0);

   /* use following line to set range to 0..1V */
   //b->SetInputRange(0.5);
   
   /* use following line to turn on the internal 100 MHz clock connected to all channels  */
//   b->EnableTcal(1);

	 /* during readout: 0=stop domino, 1=keep domino*/
	 b->SetDominoActive(0);

//   b->SetTriggerLevel(0.05);            // 0.05 V
//   b->SetTriggerPolarity(false);        // positive edge
   
   /// Pulser \/ -300mV 
   b->SetTriggerPolarity(true);        // true = negaitive edge
   b->SetIndividualTriggerLevel(0, -0.2);
   b->SetIndividualTriggerLevel(1, -0.2);
   b->SetIndividualTriggerLevel(2, -0.2);
   b->SetIndividualTriggerLevel(3, -0.2);
   
   b->SetTriggerDelayNs(0);             // zero ns trigger delay
   b->SetLatchDelayNs(100);
   printf("Latch delay: %d\n",b->GetLatchDelay());
//   b->SetLatchDelay(1000);
//   printf("Latch delay: %d\n",b->GetLatchDelay());
   
  
	 b->SetFixedRateDelay(100);
   b->SetTriggerSource(64);
//   b->SetTriggerSource(128);
   
   b->SetStatusSource(3);

/*
FW version: 533E
0   1     Or analog intput 1                              
1   2     Or analog intput 2                              
2   4     Or analog intput 3                              
3   8     Or analog intput 4                              
4   16    Or external trigger
5   32    (Or to ext-out)
6   64    Doublet 1234E
7   128   Tripplet 1234E                                       
8   256   And analog input 1                              
9   512   And analog input 2                              
10  1024	And analog input 3                              
11  	11  And analog input 4                              
12  	12  And external trigg   
13
14
15

*/


   b->PrintRegisters();

	//// CONFIG FINISHED


   /* open file to save waveforms */
   f = fopen("data.txt", "w");
   if (f == NULL) {
      perror("ERROR: Cannot open file \"data.txt\"");
      return 1;
   }
      
   fprintf(f, "# Event ID  t1[ns]  u1[mV]  u2[mV]  u3[mV]  u4[mV]\n"); 
   printf(" TriggerTime: %llu\n", b->GetFirstTriggerTime());

	 printf("Start Readout\n");
	 b->EnableTrigger(1, 0);           // enable hardware trigger

   /* repeat ten times */
   for (j=0 ; j<10 ; j++) {

      /* start board (activate domino wave) */
      b->StartDomino();

      /* wait for trigger */
      printf("Waiting for trigger...");
      
			fflush(stdout);


			//Trigger on CTRL-C
			if (catch_signal_flag!=0) {
				b->SoftTrigger();
				catch_signal_flag=0;
			}

//			printf("; post dom. TrTime: %llu ;", b->GetFirstTriggerTime());
      while (b->IsBusy() && catch_signal_flag==0)
      {
//   printf(" while busy TriggerTime: %llu\n", b->GetFirstTriggerTime());
      }
//      while (b->IsBusy())

      /* read all waveforms */
      b->TransferWaves(0, 8);

      /* read time (X) array of first channel in ns */
      b->GetTime(0, 0, b->GetTriggerCell(0), time_array[0]);

      /* decode waveform (Y) array of first channel in mV */
      b->GetWave(0, 0, wave_array[0]);

      /* read time (X) array of second channel in ns
       Note: On the evaluation board input #1 is connected to channel 0 and 1 of
       the DRS chip, input #2 is connected to channel 2 and 3 and so on. So to
       get the input #2 we have to read DRS channel #2, not #1. */
      b->GetTime(0, 2, b->GetTriggerCell(0), time_array[1]);

      /* decode waveform (Y) array of second channel in mV */
      b->GetWave(0, 2, wave_array[1]);

      /* Save waveform: X=time_array[i], Yn=wave_array[n][i] */
//      fprintf(f, "Event #%d ----------------------\n  t1[ns]  u1[mV]  t2[ns] u2[mV]\n", j);
      for (i=0 ; i<1024 ; i++)
         fprintf(f, "%d %7.3f %7.1f %7.1f %7.1f %7.1f\n", j, time_array[0][i], wave_array[0][i], wave_array[1][i], wave_array[2][i], wave_array[3][i]);

      /* print some progress indication */
      printf("\rEvent #%d read successfully\n", j);
      for(unsigned int channel=0; channel<=5; channel++) {
         printf("%5.5i\n",b->GetScaler(channel));
      }
      printf(" TriggerTime: %llu\n", b->GetFirstTriggerTime());
      printf(" TriggerTime: %llu\n", b->GetFirstTriggerTime());
      printf(" TriggerTime: %llu\n", b->GetFirstTriggerTime());
      printf(" TriggerTime: %llu\n", b->GetFirstTriggerTime());
      printf("FiredTrigger: %d\n", b->GetFiredTrigger());
   }

   fclose(f);
   
   /* delete DRS object -> close USB connection */
   delete drs;
}
