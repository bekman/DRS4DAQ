/********************************************************************\

  Name:         ReadWriteAddr.cpp
  Created by:   Ilja Bekman


\********************************************************************/
#include <signal.h>
#include <math.h>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>

#define DIR_SEPARATOR '/'

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "strlcpy.h"
#include "DRS.h"
//#include "bitutils.h"

/*------------------------------------------------------------------*/
int catch_signal_flag=0;
void catch_signal(int sig){catch_signal_flag=1;};

int main(int argc, char* argv[])
{
	signal(SIGINT, catch_signal);
  DRS *drs;
  DRSBoard *b;
	//   float time_array[8][1024];
	//   float wave_array[8][1024];
  FILE  *f;

	uint32_t REG_ADDR;
	uint32_t DATA;
	uint32_t READ_DATA;
	uint32_t BIT;

//	if (argc<2){
//	 	printf("Give address in hex without 0x");
//		return -1;
//	}
//	REG_ADDR = strtol(argv[1],NULL,16);
	//		timeForSweep = (int)strtoul(argv[2], 0, 0);


  /* do initial scan */
  drs = new DRS();


  /* continue working with first board only */
  b = drs->GetBoard(0);


  unsigned char buffer[2];
  unsigned short d;

	char input[256];

  b->Init();
	b->EnableTrigger(0, 0);           // enable hardware trigger

	b->PrintRegisters();

	printf("ADDR in hex, BIT in dec (-1 for all), DATA in hex\n");
	while ( true && catch_signal_flag==0) 
	{
		fgets(input, sizeof(input), stdin);
	 	REG_ADDR = strtol(input,NULL,16);

	 	fgets(input, sizeof(input), stdin);
	 	BIT = strtol(input,NULL,10);

	 	fgets(input, sizeof(input), stdin);
	 	DATA = strtol(input,NULL,16);

		printf("%d %d %d\n",REG_ADDR,BIT,DATA);

	 	if ( catch_signal_flag==0 ){
	 		if (BIT >= 0){
         b->Read(T_CTRL, &READ_DATA, REG_ADDR, 2);
         printf("%04x -> ",READ_DATA);
         if (DATA>0)
	         DATA = READ_DATA | (1<<BIT); //set
	       else
	         DATA = READ_DATA & ~(1<<BIT); //clear
         printf("%04x",DATA);
         b->Write(T_CTRL, REG_ADDR, &DATA, 2);
			}else{
	 			b->Write(T_CTRL, REG_ADDR, &DATA, 2);
			}
		}		
		b->PrintRegisters();

	}

  /* delete DRS object -> close USB connection */
  delete drs;
  return 0;
}
