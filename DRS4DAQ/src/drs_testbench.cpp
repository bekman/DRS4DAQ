/********************************************************************\

  Name:         drs_testbench.cpp
  Created by:   Merlin Schaufel,based on drs_exam_multi.cpp by Stefan Ritt

  Contents:     Simple app. to test different reg. configurations

  $Id: based on drs_exam_multi.cpp 21509 2014-10-15 10:11:36Z ritt $

\********************************************************************/


/*
TriggerSource FW version: 533E
Bit  Int	Function
0    1	    Or analog intput 1                              
1    2	    Or analog intput 2                              
2    4	    Or analog intput 3                              
3    8	    Or analog intput 4                              
4    16	    Or external trigger                              
5    32	    Or status input

6    64	    Pair (ABCDS)                              
7    128    Triplet    (ABCDS)
                                                                                  
8    256    And analog input 1                              
9    512    And analog input 2                              
10   1024   And analog input 3                              
11   2048   And analog input 4                              
12   4096   And external trigger
13   8192   And status input
*/

/*
StatusSource FW version: 533E
Bit  Int	Function
0    1		Or analog input 1 
1    2		Or analog input 2
2    4		Or analog input 3
3    8		Or analog input 4
4    16		Or external trigger
*/

#include <signal.h>
#include <math.h>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <sys/time.h>
#define DIR_SEPARATOR '/'

#endif
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "strlcpy.h"
#include "DRS.h"

/*-----------------------f-------------------------------------------*/
int catch_signal_flag=0;
void catch_signal(int sig){catch_signal_flag=1;};

int main()
{
	signal(SIGINT, catch_signal);
	int i;
	int firmware_revision;
	DRS *drs;
	DRSBoard *b, *mb;
	std::string command;	
	int tr_reg,ftr_reg,st_reg;
	int debug_flag = 1;
	timeval start, end;
	command = "r";
	/* do initial scan, sort boards accordning to their serial numbers */
	drs = new DRS();
	drs->SortBoards();

	/* show any found board(s) */
	for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
		b = drs->GetBoard(i);
		printf("Found DRS4 evaluation board, serial #%d, firmware revision %d = %X\n", 
		b->GetBoardSerialNumber(), b->GetFirmwareVersion(), b->GetFirmwareVersion());
		if (b->GetBoardType() < 8) {
			printf("Found pre-V4 board, aborting\n");
			return 0;
		}
		if (i == 0)
			firmware_revision = b->GetFirmwareVersion();
		else
		{
			if (firmware_revision != b->GetFirmwareVersion())
			{
				printf ("Please connect only evaluation boards with the same firmware rev.\n");			
				return 0;
			}
		}
   	}
   
   	/* exit if no board found */
	if (drs->GetNumberOfBoards() == 0) {
		printf("No DRS4 evaluation board found\n");
		return 0;
   	}

   	/* set single flag if only one board found */
   	if (drs->GetNumberOfBoards() == 1) {
		printf("Only one DRS4 evaluation board found!\n");
		//return 0;
   	}
   
   	/* use first board with highest serial number as the master board */
   	mb = drs->GetBoard(0);
   
   /* common configuration for all boards */
   	for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
      	
		b = drs->GetBoard(i);
		b->Init(); //initialize board
		b->PrintRegisters();
		b->EnableTrigger(0, 0); //disable hardware trigger
      		b->SetFrequency(2, true); //set sampling frequency   
		b->SetTranspMode(0); // disable transparent mode
      		b->SetInputRange(0); // set input range to -0.5V ... +0.5V

   		b->SetTriggerPolarity(true);        // true = negative edge
   		b->SetIndividualTriggerLevel(0, -0.25);
   		b->SetIndividualTriggerLevel(1, -0.25);
   		b->SetIndividualTriggerLevel(2, -0.25);
   		b->SetIndividualTriggerLevel(3, -0.25);
   
   		b->SetTriggerDelayNs(100);             // 100 ns trigger delay
   		if (firmware_revision > 21305) 
		{
			b->SetLatchDelayNs(0);
   			printf("Latch delay: %d\n",b->GetLatchDelay());
		}
   		//b->SetTriggerSource(64);
  		//b->SetStatusSource(3);  
		if (i == 0) 
		{
    		/* master board: */
			//b->SetFixedRateDelay(100);
		} 
		else 
		{
    			/* slave boards: */
		}
		b->PrintRegisters();
	//// CONFIG FINISHED
	}

   	while((command != "a")&&(command != "abort")){
   	
		std::cout<<"Command:";
   		std::cin>>command;
		std::cout<<command;
		std::cout<<std::endl;
	
		if ((command == "h") || (command == "help"))
		{
	   		if (firmware_revision > 21305) 
			{		
				std::cout<<"List of commands:"<<std::endl;
				std::cout<<"h or help: Show list of commands"<<std::endl;
				std::cout<<"t or trigger: Start trigger/status test"<<std::endl;
				std::cout<<"		-->Abort waiting for trig. with Strg-C"<<std::endl;
				std::cout<<"f or fixrate: Start fixed rate trigger test"<<std::endl;
				std::cout<<"		-->Abort waiting for trig. with Strg-C"<<std::endl;
				std::cout<<"a or abort: End programm"<<std::endl;
			}
			else
			{		
				std::cout<<"List of commands:"<<std::endl;
				std::cout<<"h or help: Show list of commands"<<std::endl;
				std::cout<<"t or trigger: Start trigger"<<std::endl;
				std::cout<<"		-->Abort waiting for trig. with Strg-C"<<std::endl;
				std::cout<<"a or abort: End programm"<<std::endl;
			}
	
		}
	  	if (firmware_revision > 21305) 
		{	
			if((command == "f")||(command == "fixrate"))
			{		
		
				int count = 0;
				double start_stop_time;
				for (i=0 ; i<drs->GetNumberOfBoards() ; i++) 
				{	
					std::cout<<"Fixed Rate Ticks SN"<<drs->GetBoard(i)->GetBoardSerialNumber()<<" :";
   					std::cin>>ftr_reg;
					std::cout<<ftr_reg;
					std::cout<<std::endl;
   					drs->GetBoard(i)->SetTriggerSource(17);
  					drs->GetBoard(i)->SetFixedRateDelay(ftr_reg);  
  					drs->GetBoard(i)->SetStatusSource(0);  
				}
				std::cout <<"Set debug flag: ";
				std::cin >> debug_flag;
				std::cout << debug_flag;
				std::cout << std::endl;
				for (i=drs->GetNumberOfBoards()-1 ; i>=0 ; i--)
					drs->GetBoard(i)->EnableTrigger(1, 0);           // enable hardware trigger

				while(!catch_signal_flag)
				{	
					count++;
      				
					for (i=drs->GetNumberOfBoards()-1 ; i>=0 ; i--)
         					drs->GetBoard(i)->StartDomino();
	
					gettimeofday(&start, 0);
      					while (mb->IsBusy() && !catch_signal_flag){}
					gettimeofday(&end, 0);
	
					if (debug_flag){		
						start_stop_time = (double) end.tv_sec + (double) end.tv_usec/1000000.0 - (double) start.tv_sec - (double) start.tv_usec/1000000.0;
					std::cout<<"Period: "<<start_stop_time<<std::endl;
					for (i=0; i<drs->GetNumberOfBoards(); i++)
					{
						printf("Board %d: TimeID_%lld Trigger_",drs->GetBoard(i)->GetBoardSerialNumber(),drs->GetBoard(i)->GetFirstTriggerTime());	
						drs->GetBoard(i)->GetFiredTrigger();
					}
				}
			}
			catch_signal_flag = 0;
		}
	}

	if ((command == "t")||(command == "trigger"))
	{
		double start_stop_time;
		for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {	
			std::cout<<"Trigger reg board SN"<<drs->GetBoard(i)->GetBoardSerialNumber()<<" :";
   			std::cin>>tr_reg;
			std::cout<<tr_reg;
			std::cout<<std::endl;
			if (firmware_revision > 21305) 
			{	
				std::cout<<"Status reg board SN"<<drs->GetBoard(i)->GetBoardSerialNumber()<<" :";
				std::cin>>st_reg;
				std::cout<<st_reg;
				std::cout<<std::endl;
  				drs->GetBoard(i)->SetStatusSource(st_reg); 
			} 
			//std::cout<<"Latch (ticks) board SN"<<drs->GetBoard(i)->GetBoardSerialNumber()<<" :";
			//std::cin>>lt_reg;
			//std::cout<<lt_reg;
			//std::cout<<std::endl;

   			drs->GetBoard(i)->SetTriggerSource(tr_reg);

  			//drs->GetBoard(i)->SetLatchDelay(lt_reg);  
			drs->GetBoard(i)->EnableTrigger(1, 0);           // enable hardware trigger
		}

      		for (i=drs->GetNumberOfBoards()-1 ; i>=0 ; i--)
         		drs->GetBoard(i)->StartDomino();

      		printf("Waiting for trigger...");
  		fflush(stdout);	
		gettimeofday(&start, 0);
      		while (mb->IsBusy() && catch_signal_flag==0){
      		}
		gettimeofday(&end, 0);
		//Trigger on CTRL-C
		start_stop_time = (double) end.tv_sec + (double) end.tv_usec/1000000.0 - (double) start.tv_sec - (double) start.tv_usec/1000000.0;
		std::cout<<"Time: "<<start_stop_time<<"s"<<std::endl;
		
		if (catch_signal_flag!=0) {
			mb->SoftTrigger();
			}
		if (catch_signal_flag!=0)
			printf("Trigger failed");
		else
			printf("Triggered!");
		for (i=0; i<drs->GetNumberOfBoards(); i++){
			
			if (firmware_revision > 21305) 
			{
				printf("Board %d: TimeID_%lld Trigger_",drs->GetBoard(i)->GetBoardSerialNumber(),drs->GetBoard(i)->GetFirstTriggerTime());
				drs->GetBoard(i)->GetFiredTrigger();
			}
		}
		catch_signal_flag=0;
	}
   }
}
