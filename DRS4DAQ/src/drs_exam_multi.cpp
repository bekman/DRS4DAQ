/********************************************************************\

  Name:         drs_exam_multi.cpp
  Created by:   Stefan Ritt

  Contents:     Simple example application to read out a several
                DRS4 evaluation board in daisy-chain mode

  $Id: drs_exam_multi.cpp 21509 2014-10-15 10:11:36Z ritt $

\********************************************************************/
#include <signal.h>
#include <math.h>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>

#define DIR_SEPARATOR '/'

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "strlcpy.h"
#include "DRS.h"

/*------------------------------------------------------------------*/
int catch_signal_flag=0;
void catch_signal(int sig){catch_signal_flag=1;};
int main()
{
   signal(SIGINT, catch_signal);
   int i, ev, k;
   DRS *drs;
   DRSBoard *b, *mb;
   float time_array[8][1024];
   float wave_array[8][1024];
   FILE  *f, *ft;


   /* do initial scan, sort boards accordning to their serial numbers */
   drs = new DRS();
   drs->SortBoards();

   /* show any found board(s) */
   for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
      b = drs->GetBoard(i);
      printf("Found DRS4 evaluation board, serial #%d, firmware revision %d = %X\n", 
         b->GetBoardSerialNumber(), b->GetFirmwareVersion(), b->GetFirmwareVersion());
      if (b->GetBoardType() < 8) {
         printf("Found pre-V4 board, aborting\n");
         return 0;
      }
   }
   
   /* exit if no board found */
   if (drs->GetNumberOfBoards() == 0) {
      printf("No DRS4 evaluation board found\n");
      return 0;
   }

   /* exit if only one board found */
   if (drs->GetNumberOfBoards() == 1) {
      printf("Only one DRS4 evaluation board found, please use drs_exam program\n");
      return 0;
   }
   
   /* use first board with highest serial number as the master board */
   mb = drs->GetBoard(0);
   
   /* common configuration for all boards */
   for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
      b = drs->GetBoard(i);
      
      /* initialize board */
      b->Init();
			b->PrintRegisters();
      
			b->EnableTrigger(0, 0);           // enable hardware trigger

      /* set sampling frequency */
      b->SetFrequency(5, true);
      
			/* enable transparent mode needed for analog trigger */
			b->SetTranspMode(1);

       /* set input range to -0.5V ... +0.5V */
      b->SetInputRange(0);

   /* use following line to set range to 0..1V */
   //b->SetInputRange(0.5);
   
   /* use following line to turn on the internal 100 MHz clock connected to all channels  */
//   b->EnableTcal(1);

	 /* during readout: 0=stop domino, 1=keep domino*/
	 b->SetDominoActive(0);

//   b->SetTriggerLevel(0.05);            // 0.05 V
//   b->SetTriggerPolarity(false);        // positive edge
   
   /// Pulser \/ -300mV 
   b->SetTriggerPolarity(true);        // true = negaitive edge
   b->SetIndividualTriggerLevel(0, -0.2);
   b->SetIndividualTriggerLevel(1, -0.2);
   b->SetIndividualTriggerLevel(2, -0.2);
   b->SetIndividualTriggerLevel(3, -0.2);
   
   b->SetTriggerDelayNs(0);             // zero ns trigger delay
   b->SetLatchDelayNs(100);
   printf("Latch delay: %d\n",b->GetLatchDelay());
//   b->SetLatchDelay(1000);
//   printf("Latch delay: %d\n",b->GetLatchDelay());
   
  
   b->SetTriggerSource(64);
//   b->SetTriggerSource(128);
   
   b->SetStatusSource(3);

/*
FW version: 533E
0   1     Or analog intput 1                              
1   2     Or analog intput 2                              
2   4     Or analog intput 3                              
3   8     Or analog intput 4                              
4   16    Or external trigger
5   32    (Or to ext-out)
6   64    Doublet 1234E
7   128   Tripplet 1234E                                       
8   256   And analog input 1                              
9   512   And analog input 2                              
10  1024	And analog input 3                              
11  	11  And analog input 4                              
12  	12  And external trigg   
13
14
15

*/
      
		if (i == 0) {
    		/* master board: */
				b->SetFixedRateDelay(100);
		} else {
    		/* slave boards: */
		}
		b->PrintRegisters();

		//// CONFIG FINISHED

   }

   /* open files to save waveforms and trigger data */
   f = fopen("data.txt", "w");
   if (f == NULL) {
      perror("ERROR: Cannot open file \"data.txt\"");
      return 1;
   }
   
   ft = fopen("data_trig.txt", "w");
   if (ft == NULL) {
      perror("ERROR: Cannot open file \"data.txt\"");
      return 1;
   }


		
	 for (i=0 ; i<drs->GetNumberOfBoards() ; i++)
      drs->GetBoard(i)->PrintRegisters(ft);
  

   fprintf(f, "# ID\tBrd\tt1[ns]\tu1[mV]\tt2[ns]\tu2[mV]\tt3[ns]\tu3[mV]\tt4[ns]\tu4[mV]\n"); 
   fprintf(ft, "# ID\tBrd\tT[ns]\tSRC\n"); 
   
   printf(" TriggerTime: %llu\n", mb->GetFirstTriggerTime());

	 printf("Start Readout\n");
   for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
      drs->GetBoard(i)->EnableTrigger(1, 0);           // enable hardware trigger
 	 }

   /* repeat ten times */
   for (ev=0 ; ev<10 ; ev++) {

      /* start boards (activate domino wave), master is last */
      for (i=drs->GetNumberOfBoards()-1 ; i>=0 ; i--)
         drs->GetBoard(i)->StartDomino();

      /* wait for trigger on master board */
      printf("Waiting for trigger...");
      fflush(stdout);
			
			//Trigger on CTRL-C
			if (catch_signal_flag!=0) {
				mb->SoftTrigger();
				catch_signal_flag=0;
			}

//			printf("; post dom. TrTime: %llu ;", b->GetFirstTriggerTime());
      while (mb->IsBusy() && catch_signal_flag==0)
      {
//   printf(" while busy TriggerTime: %llu\n", b->GetFirstTriggerTime());
      }
//      while (b->IsBusy())


      for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
         b = drs->GetBoard(i);

/*			
         if (b->IsBusy()) {
            ev--; // skip that event, must be some fake trigger
            break;
         }
*/       
         /* read all waveforms from all boards */
         b->TransferWaves(0, 8);
         
         for (k=0 ; k<4 ; k++) {
						/*b->GetTime(0, k*2, b->GetTriggerCell(0), time_array[k]);
       			Note: On the evaluation board input #1 is connected to channel 0 and 1 of
       			the DRS chip, input #2 is connected to channel 2 and 3 and so on. So to
       			get the input #2 we have to read DRS channel #2, not #1. */
            /* read time (X) array in ns */

            /* decode waveform (Y) arrays in mV */
            b->GetWave(0, k*2, wave_array[k]);
         }

         /* Save waveform: X=time_array[ev], Channel_n=wave_array[n][ev] */
//         fprintf(f, "Board #%d ---------------------------------------------------\n t1[ns]  u1[mV]  t2[ns]  u2[mV]  t3[ns]  u3[mV]  t4[ns]  u4[mV]\n", b->GetBoardSerialNumber());
				 int bd = b->GetBoardSerialNumber();
				 long ttime = b->GetFirstTriggerTime();
				 int tsrc =  b->GetFiredTrigger(); 
				 for (k=0 ; k<1024 ; k++)
            fprintf(f, "%d %d %7.3f %7.1f %7.3f %7.1f %7.3f %7.1f %7.3f %7.1f\n",
										ev, bd,
                    time_array[0][k], wave_array[0][k],
                    time_array[1][k], wave_array[1][k],
                    time_array[2][k], wave_array[2][k],
                    time_array[3][k], wave_array[3][k]);
         fprintf(ft, "%d %d %d %d", ev, bd, ttime, tsrc);
      } //for-all readout

      /* print some progress indication */
      printf("\rEvent #%d read successfully\n", ev);
   }

   fclose(ft);
   fclose(f);
   
   printf("Program finished.\n");

   /* delete DRS object -> close USB connection */
   delete drs;
}
